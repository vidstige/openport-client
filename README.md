Openport - Client
===========

The official client of the Openport project.

Allows you to share ports to the internet using public reverse ssh servers.

Checkout https://openport.io/wiki for more information.